package main

import (
	"errors"
)

// The two stones and the empty intersection are encoded with specific bytes.
const (
	Empty byte = '.'
	Black byte = 'b'
	White byte = 'w'
)

func SwapColor(stone byte) (byte, error) {
	switch stone {
	case Black:
		return White, nil
	case White:
		return Black, nil
	default:
		return stone, errors.New("not a stone")
	}
}

type Coordinate struct {
	Row uint
	Col uint
}

type GoMove struct {
	Stone byte
	At    Coordinate
}

// Return true if the coordinate pair is a valid intersection in a
// game with the given dimensions.
func IsOnBoard(maxCoord Coordinate, thisCoord Coordinate) bool {
	return thisCoord.Row <= maxCoord.Row && thisCoord.Col <= maxCoord.Col
}

// The game itself is stored as a byte array of length rows * cols.
// For convenience, the neighbors of each intersection are calculated
// and stored as an array of arrays of indices.
// Additionally, the history of the board state is stored internally in
// order to enforce ko or superko.
type GoGame struct {
	// Maximum row and column are stored in a coordinate pair.
	MaxCoord  Coordinate
	Board     []byte
	history   map[string]struct{}
	neighbors [][]uint
}

// To help construct the array of neighboring intersections, we use an array
// of "displacements" which can be added to a given coordinate pair.
var neighborDisplacements = [4][2]int{
	{1, 0},
	{-1, 0},
	{0, 1},
	{0, -1},
}

// Construct a new, empty game with the given dimensions.
func NewGoGame(rows uint, cols uint) *GoGame {
	// Initialize the game board as an array of stones.
	board := make([]byte, rows*cols)
	for i := uint(0); i < rows*cols; i++ {
		board[i] = Empty
	}

	// Initialize the game's history with this empty board.
	history := make(map[string]struct{})
	history[string(board)] = struct{}{}

	maxCoord := Coordinate{Row: rows - 1, Col: cols - 1}

	// Cache a list of neighbors for each intersection.
	neighbors := make([][]uint, rows*cols)
	for row := uint(0); row < rows; row++ {
		for col := uint(0); col < cols; col++ {
			offset := offsetOf(maxCoord, Coordinate{Row: row, Col: col})

			for _, displacement := range neighborDisplacements {
				displacedRow := uint(int(row) + displacement[0])
				displacedCol := uint(int(col) + displacement[1])
				displacedCoord := Coordinate{Row: displacedRow, Col: displacedCol}

				if IsOnBoard(maxCoord, displacedCoord) {
					neighbors[offset] = append(
						neighbors[offset],
						offsetOf(maxCoord, displacedCoord),
					)
				}
			}
		}
	}

	return &GoGame{
		MaxCoord:  maxCoord,
		Board:     board,
		history:   history,
		neighbors: neighbors,
	}
}

// Get the intersection at the given coordinate.
func (game *GoGame) Get(at Coordinate) byte {
	return game.getOffset(offsetOf(game.MaxCoord, at))
}

// Play the given stone at the given intersection.
// Returns an array of stones added, an array of stones removed, and
// potentially an error value.
func (game *GoGame) Play(move GoMove) ([]GoMove, []GoMove, error) {
	oppositeColor, err := SwapColor(move.Stone)
	if err != nil {
		// An invalid stone was provided.
		return nil, nil, err
	}

	if !IsOnBoard(game.MaxCoord, move.At) {
		// The given intersection is invalid.
		return nil, nil, errors.New("invalid intersection")
	}

	if game.Get(move.At) != Empty {
		// The intersection is already occupied.
		return nil, nil, errors.New("intersection is already occupied")
	}

	var added []GoMove
	var removed []GoMove

	moveOffset := offsetOf(game.MaxCoord, move.At)

	// Place the stone.
	game.justPlaceStone(move.Stone, moveOffset)
	added = append(added, move)

	// Potentially capture any of the opponent's chains around this stone.
	for _, offset := range game.neighbors[moveOffset] {
		if game.getOffset(offset) == oppositeColor {
			// This is an opponent's stone.
			captured := game.maybeCaptureChain(fromOffset(game.MaxCoord, offset))

			for _, capture := range captured {
				removed = append(removed, capture)
			}
		}
	}

	// Potentially capture any of the player's own chains around this stone.
	for _, offset := range game.neighbors[moveOffset] {
		if game.getOffset(offset) == move.Stone {
			// This is one of the player's own stones.
			captured := game.maybeCaptureChain(fromOffset(game.MaxCoord, offset))

			for _, capture := range captured {
				removed = append(removed, capture)
			}
		}
	}

	stringified := string(game.Board)

	if _, ok := game.history[stringified]; ok {
		// This board state has occurred before.
		return nil, nil, errors.New("move violates superko")
	}

	// Add this board state to the history.
	game.history[stringified] = struct{}{}

	return added, removed, nil
}

// Given a coordinate pair and the dimensions of the game,
// construct the index of that intersection as it
// exists inside the one-dimensional game board string.
func offsetOf(maxCoord Coordinate, thisCoord Coordinate) uint {
	return (maxCoord.Col+1)*thisCoord.Row + thisCoord.Col
}

// Given a one-dimensional index of an intersection inside the game board
// string, construct the equivalent coordinate pair.
func fromOffset(maxCoord Coordinate, offset uint) Coordinate {
	return Coordinate{Row: offset / (maxCoord.Col + 1), Col: offset % (maxCoord.Col + 1)}
}

// Get the intersection at the given offset.
func (game *GoGame) getOffset(offset uint) byte {
	return game.Board[offset]
}

// Place a stone at the given intersection, without also performing
// any captures or validation.
func (game *GoGame) justPlaceStone(stone byte, offset uint) {
	game.Board[offset] = stone
}

// A chain of stones along with the stones neighboring the chain
// are paired together here and called a "group."
type group struct {
	chain     map[GoMove]struct{}
	neighbors map[GoMove]struct{}
}

// Find the chain of which the given intersection is a member, along with
// the set of neighboring stones around this chain.
func (game *GoGame) findGroup(startCoord Coordinate) group {
	// Get the color of the chain that we're exploring.
	chainColor := game.Get(startCoord)
	// HACK: Use a channel of moves as a queue of intersections to visit.
	toVisit := make(chan GoMove, game.MaxCoord.Row*game.MaxCoord.Col)
	// HACK: Also make a set of all moves that have ever been queued.
	//       This is to avoid queuing an intersection more than once.
	queued := make(map[GoMove]struct{})
	// Add the given intersection to the queue.
	start := GoMove{Stone: chainColor, At: startCoord}
	toVisit <- start
	queued[start] = struct{}{}
	// Create the set of intersections which are in the chain.
	chain := make(map[GoMove]struct{})
	// Create the set of neighboring intersections around the chain.
	neighbors := make(map[GoMove]struct{})

	// Loop until the queue is empty.
	for 0 < len(toVisit) {
		// Pop a neighbor out of the queue.
		intersection := <-toVisit
		// We're visiting this intersection, so it must be part of the chain.
		chain[intersection] = struct{}{}

		thisOffset := offsetOf(game.MaxCoord, intersection.At)

		// Peek at each of the neighbors of this intersection.
		for _, neighborOffset := range game.neighbors[thisOffset] {
			neighborCoord := fromOffset(game.MaxCoord, neighborOffset)
			// Construct a move for this neighboring intersection.
			move := GoMove{
				Stone: game.getOffset(neighborOffset),
				At:    neighborCoord,
			}

			if _, ok := chain[move]; ok {
				// This neighbor is already a member of the chain.
				continue
			}

			if _, ok := neighbors[move]; ok {
				// This neighbor is already a member of the neighbors.
				continue
			}

			if game.Get(neighborCoord) != chainColor {
				// This intersection is a new neighbor of the whole chain.
				neighbors[move] = struct{}{}
				continue
			}

			// At this point, this intersection is not already a member of
			// the chain or the neighbors and its color is equal to the chain
			// color. All that remains is to see if it's already queued.

			if _, ok := queued[move]; !ok {
				// The move has never been queued before.
				// Add it to the queue:
				toVisit <- move
				// Record that it has been queued:
				queued[move] = struct{}{}
			}
		}
	}

	return group{chain: chain, neighbors: neighbors}
}

// Potentially capture the chain of which the given intersection
// is a member. Returns any stones that were removed.
func (game *GoGame) maybeCaptureChain(startCoord Coordinate) []GoMove {
	thisGroup := game.findGroup(startCoord)
	hasLiberty := false

	for neighbor := range thisGroup.neighbors {
		if neighbor.Stone == Empty {
			// This chain cannot be captured, because it has at least one liberty.
			hasLiberty = true
			break
		}
	}

	if !hasLiberty {
		// This chain has no liberties, and therefore must be captured.
		// Make an array which will contain the removed stones.
		captured := make([]GoMove, len(thisGroup.chain))
		for member := range thisGroup.chain {
			captured = append(captured, member)
			game.justPlaceStone(Empty, offsetOf(game.MaxCoord, member.At))
		}

		// Return the chain that was captured.
		return captured
	}

	// Nothing was captured, so return nil.
	return nil
}
