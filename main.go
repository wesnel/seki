package main

import (
	"encoding/json"
	"flag"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/rs/cors"
	"log"
	"net/http"
	"time"
)

// Command line options.
var addr = flag.String("addr", ":8080", "http service address")

type GetGameResponse struct {
	Properties GoGamePropertiesResponse
	Board      string
}

func games(
	serverManager *ServerManager,
	writer http.ResponseWriter,
	request *http.Request,
) {
	switch request.Method {
	case "POST":
		gameUUID, err := uuid.NewRandom()
		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		gameID := gameUUID.String()
		request := CreateGameRequest{
			GameID: gameID,
			Properties: GoGameProperties{
				Rows:        19,
				Cols:        19,
				CurrentTurn: Black,
			},
		}

		response := serverManager.CreateGame(request)
		log.Print("created game ", gameID)

		js, err := json.Marshal(response)
		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		writer.Header().Set("Content-Type", "application/json")
		writer.Write(js)
		break
	case "GET":
		gameID := request.URL.Query().Get("id")
		if gameID == "" {
			http.Error(writer, "missing id parameter", http.StatusBadRequest)
			return
		}

		log.Print("received request to view game ", gameID)

		serverManager.mtx.Lock()
		defer serverManager.mtx.Unlock()

		gameManager, exists := serverManager.GetGameManager(gameID)
		if !exists {
			http.Error(writer, "game does not exist", http.StatusNotFound)
			return
		}

		gameManager.mtx.Lock()
		defer gameManager.mtx.Unlock()

		game := gameManager.GetGame()

		response := GetGameResponse{
			Board: string(game.Board),
			Properties: GoGamePropertiesResponse{
				Rows:        game.MaxCoord.Row,
				Cols:        game.MaxCoord.Col,
				CurrentTurn: string(gameManager.currentTurn),
			},
		}

		js, err := json.Marshal(response)
		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		writer.Header().Set("Content-Type", "application/json")
		writer.Write(js)
		break
	default:
		http.Error(writer, "method not allowed", http.StatusMethodNotAllowed)
		return
	}
}

func play(
	serverManager *ServerManager,
	upgrader websocket.Upgrader,
	writer http.ResponseWriter,
	request *http.Request,
) {
	gameID := request.URL.Query().Get("id")
	if gameID == "" {
		http.Error(writer, "missing id parameter", http.StatusBadRequest)
		return
	}

	log.Print("player is attempting to join game ", gameID)

	gameManager, exists := serverManager.GetGameManager(gameID)
	if !exists {
		http.Error(writer, "game does not exist", http.StatusNotFound)
		return
	}

	log.Print("upgrading player to websocket connection for game ", gameID)

	conn, err := upgrader.Upgrade(writer, request, nil)
	if err != nil {
		return
	}

	player := Player{
		gameManager: gameManager,
		conn:        conn,
		send:        make(chan interface{}),
	}

	go player.ListenForIncoming()
	go player.ListenForOutgoing()

	log.Print("registering new player for game ", gameID)

	gameManager.RegisterPlayer(&player)

	log.Print("player has joined game ", gameID)
}

func main() {
	flag.Parse()

	// For upgrading HTTP requests to Websocket.
	upgrader := websocket.Upgrader{
		// FIXME: Not safe :)
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	// For managing all of the games.
	serverManager := NewServerManager(5 * time.Minute)

	mux := http.NewServeMux()
	mux.HandleFunc("/games", func(w http.ResponseWriter, r *http.Request) {
		games(serverManager, w, r)
	})
	mux.HandleFunc("/play", func(w http.ResponseWriter, r *http.Request) {
		play(serverManager, upgrader, w, r)
	})
	handler := cors.Default().Handler(mux)
	log.Fatal(http.ListenAndServe(*addr, handler))
}
