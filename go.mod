module gitlab.com/wesnel/seki

go 1.15

require (
	github.com/google/uuid v1.2.0
	github.com/gorilla/websocket v1.4.2
	github.com/rs/cors v1.7.0
)
