package main

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"sync"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

type Player struct {
	gameManager *GameManager
	conn        *websocket.Conn
	send        chan interface{}
}

type GoMoveRequest struct {
	Stone string
	At    Coordinate
}

func (p *Player) ListenForIncoming() {
	defer func() {
		p.gameManager.RemovePlayer(p)
		p.conn.Close()
	}()

	p.conn.SetReadLimit(maxMessageSize)
	p.conn.SetReadDeadline(time.Now().Add(pongWait))
	p.conn.SetPongHandler(func(string) error {
		p.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		_, message, err := p.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(
				err,
				websocket.CloseGoingAway,
				websocket.CloseAbnormalClosure,
			) {
				log.Print(err)
			}

			break
		}

		var moveRequest GoMoveRequest
		err = json.Unmarshal(message, &moveRequest)
		if err != nil {
			log.Print(err)
			return
		}

		if len(moveRequest.Stone) != 1 {
			log.Print("invalid move in game ", p.gameManager.gameID)
			return
		}

		log.Print("playing move in game ", p.gameManager.gameID)

		p.gameManager.PlayMove(GoMove{
			Stone: []byte(moveRequest.Stone)[0],
			At:    moveRequest.At,
		})
	}
}

func (p *Player) ListenForOutgoing() {
	ticker := time.NewTicker(pingPeriod)

	defer func() {
		ticker.Stop()
		p.conn.Close()
	}()

	for {
		select {
		case item, ok := <-p.send:
			p.conn.SetWriteDeadline(time.Now().Add(writeWait))

			if !ok {
				// The send channel was closed.
				p.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			log.Print("sending data to some player in game ", p.gameManager.gameID)
			p.conn.WriteJSON(item)
		case <-ticker.C:
			p.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := p.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

type ServerManager struct {
	mtx            sync.Mutex
	games          map[string]*GameManager
	newGameTimeout time.Duration
}

func NewServerManager(newGameTimeout time.Duration) *ServerManager {
	return &ServerManager{
		newGameTimeout: newGameTimeout,
		games:          make(map[string]*GameManager),
	}
}

type GoGameProperties struct {
	Rows        uint
	Cols        uint
	CurrentTurn byte // 'b' or 'w'
}

type CreateGameRequest struct {
	GameID     string
	Properties GoGameProperties
}

type GoGamePropertiesResponse struct {
	Rows        uint
	Cols        uint
	CurrentTurn string // "b" or "w"
}

type CreateGameResponse struct {
	GameID     string
	Properties GoGamePropertiesResponse
}

func (m *ServerManager) CreateGame(request CreateGameRequest) *CreateGameResponse {
	m.mtx.Lock()
	defer m.mtx.Unlock()

	gameID := request.GameID
	if _, alreadyExists := m.games[gameID]; !alreadyExists {
		log.Print("creating game manager for ", gameID)

		gameManager := NewGameManager(
			gameID,
			m,
			request.Properties.CurrentTurn,
			request.Properties.Rows,
			request.Properties.Cols,
		)
		m.games[gameID] = gameManager

		// Players have five minutes to join the game.
		time.AfterFunc(m.newGameTimeout, func() {
			m.EndGameIfEmpty(gameID)
		})

		return &CreateGameResponse{
			GameID: gameID,
			Properties: GoGamePropertiesResponse{
				Rows:        request.Properties.Rows,
				Cols:        request.Properties.Cols,
				CurrentTurn: string(request.Properties.CurrentTurn),
			},
		}
	}

	return nil
}

// XXX: You should have a lock on the ServerManager.mtx first.
func (m *ServerManager) GetGameManager(gameID string) (*GameManager, bool) {
	// Why can't go figure this part out without my being so obvious?
	gameManager, exists := m.games[gameID]
	return gameManager, exists
}

func (m *ServerManager) EndGameIfEmpty(gameID string) {
	m.mtx.Lock()
	defer m.mtx.Unlock()

	if gameManager, exists := m.games[gameID]; exists {
		log.Print("checking for lack of players in ", gameID)
		if gameManager.playerBlack == nil && gameManager.playerWhite == nil {
			log.Print("lack of players; killing ", gameID)
			delete(m.games, gameID)
		}
	}
}

type GameManager struct {
	mtx           sync.Mutex
	gameID        string
	game          *GoGame
	serverManager *ServerManager
	playerBlack   *Player
	playerWhite   *Player
	currentTurn   byte // 'b' or 'w'
}

func NewGameManager(
	gameID string,
	serverManager *ServerManager,
	currentTurn byte,
	rows uint,
	cols uint,
) *GameManager {
	return &GameManager{
		gameID:        gameID,
		serverManager: serverManager,
		game:          NewGoGame(rows, cols),
		playerBlack:   nil,
		playerWhite:   nil,
		currentTurn:   currentTurn,
	}
}

type RegisterPlayerResponse struct {
	ResponseType string // describes this response to the client
	PlayerColor  string
	Board        string
	Properties   GoGamePropertiesResponse
}

func (m *GameManager) RegisterPlayer(player *Player) {
	m.mtx.Lock()
	defer m.mtx.Unlock()

	var playerColor byte

	if m.playerBlack == nil {
		// Attempt to register as Black first.
		m.playerBlack = player
		playerColor = Black
		log.Print("registered Black player in ", m.gameID)
	} else if m.playerWhite == nil {
		// Next try to register as White.
		m.playerWhite = player
		playerColor = White
		log.Print("registered White player in ", m.gameID)
	}

	log.Print("sending registration response to ", playerColor, " in ", m.gameID)

	// Notify the user about the game properties.
	player.send <- RegisterPlayerResponse{
		ResponseType: "RegisterPlayerResponse",
		PlayerColor:  string(playerColor),
		Properties: GoGamePropertiesResponse{
			CurrentTurn: string(m.currentTurn),
			Rows:        m.game.MaxCoord.Row + 1,
			Cols:        m.game.MaxCoord.Col + 1,
		},
		Board: string(m.game.Board),
	}
}

func (m *GameManager) RemovePlayer(player *Player) {
	m.mtx.Lock()
	defer m.mtx.Unlock()

	if m.playerBlack == player {
		log.Print("removing player Black in ", m.gameID)
		m.playerBlack = nil
	}

	if m.playerWhite == player {
		log.Print("removing player White in ", m.gameID)
		m.playerWhite = nil
	}

	// Close the player's send channel so it knows to end its goroutine.
	close(player.send)

	// If the game is now empty, close it.
	m.serverManager.EndGameIfEmpty(m.gameID)
}

type PlayedMoveResponse struct {
	ResponseType string // describes this response to the client
	// FIXME: For convenience on the client-side, the full board
	//        is sent with each move. This would be pointless if
	//        the client is adapted to work with the Added and
	//        Removed arrays.
	Board       string
	CurrentTurn string // 'b' or 'w'
	Added       []GoMove
	Removed     []GoMove
	Error       error
}

func (m *GameManager) PlayMove(move GoMove) {
	m.mtx.Lock()

	var added []GoMove
	var removed []GoMove
	var err error

	defer func() {
		response := PlayedMoveResponse{
			ResponseType: "PlayedMoveResponse",
			CurrentTurn:  string(m.currentTurn),
			Board:        string(m.game.Board),
			Added:        added,
			Removed:      removed,
			Error:        err,
		}

		if m.playerBlack != nil {
			log.Print("sending move response to Black in ", m.gameID)
			m.playerBlack.send <- response
		}

		if m.playerWhite != nil {
			log.Print("sending move response to White in ", m.gameID)
			m.playerWhite.send <- response
		}

		m.mtx.Unlock()
	}()

	if move.Stone == m.currentTurn {
		added, removed, err = m.game.Play(move)
		if err != nil {
			log.Print(err)
			return
		}

		var nextStone byte
		nextStone, err = SwapColor(m.currentTurn)
		if err != nil {
			log.Print(err)
			return
		}

		m.currentTurn = nextStone
	}
}

// XXX: You should have a lock on the GameManager.mtx first.
func (m *GameManager) GetGame() *GoGame {
	return m.game
}
