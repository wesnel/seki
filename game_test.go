package main

import (
	"testing"
)

func TestGameDimensions(t *testing.T) {
	rows := uint(3)
	cols := uint(2)
	game := NewGoGame(rows, cols)

	if gotRows := game.MaxCoord.Row; gotRows != rows-1 {
		t.Fatalf(`game has %d rows, but wanted %d`, gotRows, rows-1)
	}

	if gotCols := game.MaxCoord.Col; gotCols != cols-1 {
		t.Fatalf(`game has %d cols, but wanted %d`, gotCols, cols-1)
	}
}

func TestNewGoGameGameIsEmpty(t *testing.T) {
	rows := uint(3)
	cols := uint(2)
	game := NewGoGame(rows, cols)

	for offset, stone := range game.Board {
		if stone != Empty {
			t.Fatalf(`stone at offset %d was %d, but wanted %d`, offset, stone, Empty)
		}
	}
}

func TestCanPlayStone(t *testing.T) {
	rows := uint(3)
	cols := uint(2)
	game := NewGoGame(rows, cols)

	row := uint(1)
	col := uint(1)
	coord := Coordinate{Row: row, Col: col}
	expectedAddedLength := 1
	expectedMove := GoMove{Stone: Black, At: coord}
	added, removed, err := game.Play(expectedMove)

	if err != nil {
		t.Fatalf(`error was %v, but wanted %v`, err, nil)
	}

	if removed != nil {
		t.Fatalf(`removed %v, but wanted %v`, removed, nil)
	}

	if len(added) != expectedAddedLength {
		t.Fatalf(`got %d added stones, but wanted %d`, len(added), expectedAddedLength)
	}

	if added[0] != expectedMove {
		t.Fatalf(`added %v, but wanted %v`, added[0], expectedMove)
	}
}

func TestGetBoardOffset(t *testing.T) {
	expected := uint(0)
	if offset := offsetOf(Coordinate{Row: 18, Col: 18}, Coordinate{Row: 0, Col: 0}); offset != expected {
		t.Fatalf(`expected offset %v, but got %v`, expected, offset)
	}

	expected = 1
	if offset := offsetOf(Coordinate{Row: 18, Col: 18}, Coordinate{Row: 0, Col: 1}); offset != expected {
		t.Fatalf(`expected offset %v, but got %v`, expected, offset)
	}

	expected = 19
	if offset := offsetOf(Coordinate{Row: 18, Col: 18}, Coordinate{Row: 1, Col: 0}); offset != expected {
		t.Fatalf(`expected offset %v, but got %v`, expected, offset)
	}

	expected = 360
	if offset := offsetOf(Coordinate{Row: 18, Col: 18}, Coordinate{Row: 18, Col: 18}); offset != expected {
		t.Fatalf(`expected offset %v, but got %v`, expected, offset)
	}
}

func TestFromBoardOffset(t *testing.T) {
	expected := Coordinate{Row: 0, Col: 0}
	if coord := fromOffset(Coordinate{Row: 18, Col: 18}, 0); coord != expected {
		t.Fatalf(`expected coordinate %v, but got %v`, expected, coord)
	}

	expected = Coordinate{Row: 0, Col: 1}
	if coord := fromOffset(Coordinate{Row: 18, Col: 18}, 1); coord != expected {
		t.Fatalf(`expected coordinate %v, but got %v`, expected, coord)
	}

	expected = Coordinate{Row: 1, Col: 0}
	if coord := fromOffset(Coordinate{Row: 18, Col: 18}, 19); coord != expected {
		t.Fatalf(`expected coordinate %v, but got %v`, expected, coord)
	}

	expected = Coordinate{Row: 18, Col: 18}
	if coord := fromOffset(Coordinate{Row: 18, Col: 18}, 360); coord != expected {
		t.Fatalf(`expected coordinate %v, but got %v`, expected, coord)
	}
}

func TestInvalidStoneFails(t *testing.T) {
	rows := uint(3)
	cols := uint(2)
	game := NewGoGame(rows, cols)

	row := uint(1)
	col := uint(1)
	coord := Coordinate{Row: row, Col: col}
	move := GoMove{Stone: byte(9), At: coord}
	added, removed, err := game.Play(move)

	if added != nil {
		t.Fatalf(`added %v, but wanted nil`, added)
	}

	if removed != nil {
		t.Fatalf(`removed %v, but wanted nil`, removed)
	}

	if err == nil {
		t.Fatal("error was nil when it should have been non-nil")
	}
}

func TestInvalidIntersectionFails(t *testing.T) {
	rows := uint(3)
	cols := uint(2)
	game := NewGoGame(rows, cols)
	move := GoMove{Stone: Black, At: Coordinate{Row: 3, Col: 2}}
	added, removed, err := game.Play(move)

	if added != nil {
		t.Fatalf(`added %v, but wanted nil`, added)
	}

	if removed != nil {
		t.Fatalf(`removed %v, but wanted nil`, removed)
	}

	if err == nil {
		t.Fatal("error was nil when it should have been non-nil")
	}
}

func TestPlayOnOccupiedIntersectionFails(t *testing.T) {
	rows := uint(3)
	cols := uint(2)
	game := NewGoGame(rows, cols)
	move := GoMove{Stone: Black, At: Coordinate{Row: 1, Col: 1}}
	game.Play(move)
	move = GoMove{Stone: White, At: Coordinate{Row: 1, Col: 1}}
	added, removed, err := game.Play(move)

	if added != nil {
		t.Fatalf(`added %v, but wanted nil`, added)
	}

	if removed != nil {
		t.Fatalf(`removed %v, but wanted nil`, removed)
	}

	if err == nil {
		t.Fatal("error was nil when it should have been non-nil")
	}

	if got := game.Get(Coordinate{Row: 1, Col: 1}); got != Black {
		t.Fatalf(`intersection was %v when it should have been %v`, got, Black)
	}
}

func compareMoveSet(
	t *testing.T,
	got map[GoMove]struct{},
	expected map[GoMove]struct{},
) {
	if len(got) != len(expected) {
		t.Fatalf(`expected %v, but got %v`, expected, got)
	}

	remainingToFind := len(expected)
	queue := make(chan GoMove, remainingToFind)
	for move := range expected {
		queue <- move
	}

	for ; 0 < remainingToFind; remainingToFind-- {
		toFind := <-queue

		if _, ok := got[toFind]; ok {
			continue
		} else {
			t.Fatalf(`expected item %v was not found`, toFind)
		}
	}
}

func TestCanFindSingleStoneChainWithFourNeighbors(t *testing.T) {
	rows := uint(19)
	cols := uint(19)
	game := NewGoGame(rows, cols)
	game.Play(GoMove{Stone: Black, At: Coordinate{Row: 9, Col: 9}})
	group := game.findGroup(Coordinate{Row: 9, Col: 9})

	expectedChain := make(map[GoMove]struct{})
	expectedChain[GoMove{Stone: Black, At: Coordinate{Row: 9, Col: 9}}] = struct{}{}
	expectedNeighbors := make(map[GoMove]struct{})
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{8, 9}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{9, 8}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{10, 9}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{9, 10}}] = struct{}{}

	compareMoveSet(t, group.chain, expectedChain)
	compareMoveSet(t, group.neighbors, expectedNeighbors)
}

func TestCanFindSingleStoneChainWithThreeNeighbors(t *testing.T) {
	rows := uint(19)
	cols := uint(19)
	game := NewGoGame(rows, cols)

	game.Play(GoMove{Stone: Black, At: Coordinate{1, 0}})
	group := game.findGroup(Coordinate{1, 0})

	expectedChain := make(map[GoMove]struct{})
	expectedChain[GoMove{Stone: Black, At: Coordinate{1, 0}}] = struct{}{}
	expectedNeighbors := make(map[GoMove]struct{})
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{0, 0}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{2, 0}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{1, 1}}] = struct{}{}

	compareMoveSet(t, group.chain, expectedChain)
	compareMoveSet(t, group.neighbors, expectedNeighbors)
}

func TestCanFindSingleStoneChainWithTwoNeighbors(t *testing.T) {
	rows := uint(19)
	cols := uint(19)
	game := NewGoGame(rows, cols)

	game.Play(GoMove{Stone: Black, At: Coordinate{0, 0}})
	group := game.findGroup(Coordinate{0, 0})

	expectedChain := make(map[GoMove]struct{})
	expectedChain[GoMove{Stone: Black, At: Coordinate{0, 0}}] = struct{}{}
	expectedNeighbors := make(map[GoMove]struct{})
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{1, 0}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{0, 1}}] = struct{}{}

	compareMoveSet(t, group.chain, expectedChain)
	compareMoveSet(t, group.neighbors, expectedNeighbors)
}

func TestCanFindChainWithLoop(t *testing.T) {
	rows := uint(19)
	cols := uint(19)
	game := NewGoGame(rows, cols)

	// Build the following stone formation:
	//     E E E E E
	// ... E B B B E ...
	// ... E B E B E ...
	// ... E B B B E ...
	//     E E E E E

	var added = [8]GoMove{
		{Stone: Black, At: Coordinate{8, 8}},
		{Stone: Black, At: Coordinate{8, 9}},
		{Stone: Black, At: Coordinate{8, 10}},
		{Stone: Black, At: Coordinate{9, 8}},
		{Stone: Black, At: Coordinate{9, 10}},
		{Stone: Black, At: Coordinate{10, 8}},
		{Stone: Black, At: Coordinate{10, 9}},
		{Stone: Black, At: Coordinate{10, 10}},
	}
	expectedChain := make(map[GoMove]struct{})

	for _, move := range added {
		expectedChain[move] = struct{}{}
		game.Play(move)
	}

	expectedNeighbors := make(map[GoMove]struct{})
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{9, 9}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{7, 8}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{7, 9}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{7, 10}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{8, 7}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{8, 11}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{9, 7}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{9, 11}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{10, 7}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{10, 11}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{11, 8}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{11, 9}}] = struct{}{}
	expectedNeighbors[GoMove{Stone: Empty, At: Coordinate{11, 10}}] = struct{}{}

	group := game.findGroup(Coordinate{10, 10})

	compareMoveSet(t, group.chain, expectedChain)
	compareMoveSet(t, group.neighbors, expectedNeighbors)
}

func TestCanCaptureSingleStone(t *testing.T) {
	rows := uint(19)
	cols := uint(19)
	game := NewGoGame(rows, cols)

	var added = [5]GoMove{
		{Stone: Black, At: Coordinate{8, 9}},
		{Stone: Black, At: Coordinate{9, 8}},
		{Stone: White, At: Coordinate{9, 9}},
		{Stone: Black, At: Coordinate{9, 10}},
		{Stone: Black, At: Coordinate{10, 9}},
	}

	for _, move := range added {
		game.Play(move)
	}

	if got := game.Get(Coordinate{8, 9}); got != Black {
		t.Fatalf(`expected intersection (%d, %d) to be %v, but got %v`, 8, 9, Black, got)
	}

	if got := game.Get(Coordinate{9, 8}); got != Black {
		t.Fatalf(`expected intersection (%d, %d) to be %v, but got %v`, 9, 8, Black, got)
	}

	if got := game.Get(Coordinate{9, 9}); got != Empty {
		t.Fatalf(`expected intersection (%d, %d) to be %v, but got %v`, 9, 9, Empty, got)
	}

	if got := game.Get(Coordinate{9, 10}); got != Black {
		t.Fatalf(`expected intersection (%d, %d) to be %v, but got %v`, 9, 10, Black, got)
	}

	if got := game.Get(Coordinate{10, 9}); got != Black {
		t.Fatalf(`expected intersection (%d, %d) to be %v, but got %v`, 10, 9, Black, got)
	}
}
